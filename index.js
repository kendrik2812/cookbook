const dotenv = require('dotenv');
const express = require('express');
const userRoute = require( './routes/userRoute.js'); 
const mongoose = require( 'mongoose');
const bodyParser = require( 'body-parser');
const cors = require('cors');
const cookBookRoute = require('./routes/cookBook.js');


const app = express();
//secure the mongodb connection
dotenv.config({path:'./config.env'});
const PORT = process.env.PORT;

//calling a connection.js file to 
require('./DB/connection');

app.use(express.json());


//size of the downloaded images are limited
app.use(bodyParser.json({limit: "20mb", extended: true}));
app.use(bodyParser.urlencoded({limit: "20mb", extended: true}));

// Middlewares
//
//

// app.get('/home', (req, res)=>{
//     res.send(`Hello from heaven`);
// })

app.use('/cookbook', cookBookRoute);
app.use('/api/user', userRoute); 

//Routes middleware
app.use(cors());

/////////////////////////////////////////////////////
//    USER Get and Post request through controller //
/////////////////////////////////////////////////////

// Controllers for get and post requests
const UserController = require('./controllers/UserControllers.js');

// Get request for user to recieve information from db
app.get("/users", UserController.all);
app.post("/users/create", UserController.create);
app.get("/users/:username", UserController.find);
app.get("/users/email/:email", UserController.findByEmail);
app.get("/users/:username/recipes", UserController.getRecipes);
app.get("/users/:username/pantry", UserController.getPantry);

// Post request for user pantry
app.post("/users/:username/pantry", UserController.addToPantry);

/////////////////////////////////////////////////////
//  Recipe Get and Post request through controller //
/////////////////////////////////////////////////////
const RecipeController = require('./controllers/RecipeController.js');

// Get request for recipes
app.get("/recipes", RecipeController.all);
app.get("/recipes/:recipe_name", RecipeController.find);

// Create a new recipe
app.post("/recipes/create", RecipeController.create);


app.listen(PORT, ()=>console.log(`Connected to the server ${PORT}`));
