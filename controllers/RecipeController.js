var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

const {registerValidation} = require('../model/validation.js');
const Recipe = require("../model/Recipe.js");
const User = require('../model/User');

let RecipeController = {
    // finds a single recipe
    find: async (req, res) => {
        let found = await Recipe.find({recipe_name: req.params.recipe_name});
        res.json(found);
    },

    // finds all recipes
    all: async (req, res) => {
        let allRecipes = await Recipe.find();
        res.json(allRecipes);
    },

    // Create a recipe
    create: async(req, res) => {
        //checking whether the user fills all the fields or not
        const {recipe_name, chef_name, ingredients, prep_time, cuisine_type, instructions, publication}= req.body;
        if(!recipe_name || !chef_name ||!ingredients ||  !prep_time || !cuisine_type || !instructions || !publication){
            return res.status(422).json({error: "Please filled the field properly"});
        }

        //create a new recipe
        const recipe = new Recipe({
            recipe_name: req.body.recipe_name,
            chef_name: req.body.chef_name,
            ingredients: req.body.ingredients,
            prep_time: req.body.prep_time,
            cuisine_type: req.body.cuisine_type,
            instructions: req.body.instructions,
            publication: req.body.publication === "true"? true: false
        });
        
        try{
            await recipe.save();     
            
            //pre saved method
            let userRecipe = await User.findOne({name: req.body.chef_name}, 'recipes');
            
            userRecipe.recipes.push(recipe._id);

            //pre saved method
            User.findOneAndUpdate({name: req.body.chef_name}, 
                {$set: userRecipe}, function(err) {console.log(err)});

            res.json(userRecipe);
            res.status = 200;

        }catch(err){
            res.status(400).send(err);

        }
    }
}

module.exports = RecipeController;