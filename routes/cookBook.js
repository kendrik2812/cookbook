const mongoose = require( 'mongoose');
const jwt = require('jsonwebtoken');
const router = require('express').Router();
const User = require('../model/User.js');
//const verify = require('./verifyToken.js');
const authentication = require('../middleware/authentication.js');

router.post('/pantry', async(req, res)=>{

    // if(ingredients === null){
    //     return res.status(400).json({error:"Please fill the fields"});
    // }
    // req.body.ingredients.map((ingredient) => )
    // const pantry = new Pantry({
    //     ingredients: mongoose.Types.ObjectId("6100b20c3e2487cad59c0013")
    // });

    try{
        const pantry = new Pantry();
        if(req.body !== null) {
            req.body.map(({ingredient, measurement}) => {
                pantry.push({
                    'ingredient': ingredient,
                    measurement: [measurement]
                })
            });
        }
        //pre saved method
        const savePantry = await pantry.save();
        res.send({'pantry': pantry})
    }catch(err){
        res.status(400).send(err);
    }
})


 module.exports = router;