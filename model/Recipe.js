const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const User = require('../model/User');

const recipeSchema = new Schema({
    recipe_name: {
        type: String,
        required: true,
        min: 5,
        max: 255
    },

    chef_name: {
        type:String,
        required: true,
        min: 5,
        max: 255

    },

    prep_time: {
        type: String,
        required: true,
        min: 5,
        max: 255
    },

    cuisine_type:{
        type:String,
        required:true,
        min:2,
        max:100
    },
   
    ingredients: [{
        ingredient: {
            type: String,
            required: false
        },
        measurement: {
            type: String,
            required: false
        }
    }],

    instructions: {
        type: String,
        required: true
    },

    publication : {
        type: Boolean,
        default: false,
        required: true
    },
});

module.exports = mongoose.model("PersonalRecipe", recipeSchema);
