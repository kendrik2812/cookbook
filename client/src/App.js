import React, { useState } from "react";
import { AppContext } from "./AppContext.js";

//Main Page

import Home from "./components/MainPage/Sections/Home/Home.js";
import Menu from "./components/MainPage/Sections/Menu/Menu.js";
import Register from "./components/MainPage/Sections/Register/Register.js";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import Signin from "./components/MainPage/Sections/Signin/Signin.js";
import MainPage from "./components/MainPage/MainPage.js";
import PrivateRoute from "./components/PrivateRoute.js";


//Child Page
import NavbarChild from "./components/ChildPage/NavbarChild/NavbarChild.js";
import Search from "./components/ChildPage/Search/Search.js";
import Pantry from "./components/ChildPage/Pantry/Pantry.js";
import Recipe from "./components/ChildPage/CreateRecipe/Recipe.js";
import DisplayResult from "./components/ChildPage/DisplayResult/DisplayResult.js";

function App() {
  const [authenticated, setAuthenticated] = useState(false);

  return (
    <AppContext.Provider value={[authenticated, setAuthenticated]}>
      <BrowserRouter>
        <Switch>
          
          <Route path="/" exact component={MainPage} />
          <Route path="/#home" exact component={Home} />
          <Route path="/#menu" exact component={Menu} />
          <Route path="/#register" exact component={Register} />
          <Route path="/#signin" exact component={Signin} />
          <PrivateRoute
            authCheck={authenticated}
            path="/navbarchild"
            exact
            component={NavbarChild}
          />
          <Route path="/search" exact component={Search} />
          <Route path="/recipe" exact component={Recipe} />
          <Route path="/pantry" exact component={Pantry} />
          <Route path="/displayresult" exact component={DisplayResult} />
        </Switch>
      </BrowserRouter>
    </AppContext.Provider>
  );
}

export default App;