import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import NavbarChild from '../NavbarChild/NavbarChild';
import "./Search.css";
import ComboBox from "../ComboBox/ComboBox.js";
import { useHistory } from "react-router-dom";


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />



let result = [];





const Search = () => {
    window.scrollTo(0, 0);
    let searchResult = [];
    const history = useHistory();
    const [meatState, setmeatState] = useState([]);
    const [vegsState, setvegsState] = useState([]);
    const [grainsState, setgrainsState] = useState([]);
    const [toggleState, setToggleState] = useState(1);
    const [data, setData] = useState();



    const toggleTab = (index) => {
        setToggleState(index);
    };
    
    const PostData = async (e) => {

        const res = await fetch("http://localhost:5000/recipes", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            },

        });
        const data = await res.json();

        let count = 0;


        console.log("result.length " + result.length);

        for (let i = 0; i < data.length; i++) {

            for (let j = 0; j < result.length; j++) {

                for (let k = 0; k < data[i].ingredients.length; k++) {

                    if (result[j] === data[i].ingredients[k].ingredient && data[i].publication == true) {
                        count++;
                    }
                }
            }
            if (count >= 2) {
                searchResult.push(data[i]);
            }
            count = 0;

        }
        setData(searchResult)
        console.log(searchResult);
        result = [];

        if (searchResult.length != 0) {
            history.push({
                pathname: "/displayresult",
                state: {
                    result: searchResult
                }
            });
        }
        else{
            window.alert("No Recipes found! Add some more ingredidents");
        }

    }

    useEffect(() => {
        let meatState = [
            { id: 1, meat: "chicken" },
            { id: 2, meat: "beef" },
            { id: 3, meat: "shrimp" },
            { id: 4, meat: "pork" }

        ];
        let vegsState = [
            { id: 5, vegs: "brocolli" },
            { id: 6, vegs: "onions" },
            { id: 7, vegs: "potato" },
            { id: 8, vegs: "garlic" },
            { id: 9, vegs: "carrots" },

        ];
        let grainsState = [
            { id: 10, grains: "noodle" },
            { id: 11, grains: "rice" },
            { id: 12, grains: "wheat" },
            { id: 13, grains: "oats" },
            { id: 14, grains: "corns" }
        ];

        setmeatState(
            meatState.map(d => {
                return {
                    select: false,
                    id: d.id,
                    meat: d.meat

                };
            })
        );
        setvegsState(
            vegsState.map(v => {
                return {
                    select: false,
                    id: v.id,
                    vegs: v.vegs

                };
            })
        );

        setgrainsState(
            grainsState.map(g => {
                return {
                    select: false,
                    id: g.id,
                    grains: g.grains

                };
            })
        );


    }, []);


    return (
        
        <div className="search-overlay" id="search">
            <NavbarChild />
            <br></br>
            <ComboBox>

            </ComboBox>
            <br></br>
            <br></br>

            <div className="container">
                <div className="bloc-tabs">
                    <td></td>
                    <td></td>
                    <tr></tr>
                    <tr></tr>
                    <td>
                        <button
                            className={toggleState === 1 ? "tabs active-tabs" : "tabs"}
                            onClick={() => toggleTab(1)}
                        >
                            Meat
                        </button>
                    </td>
                    <td>
                        <button
                            className={toggleState === 2 ? "tabs active-tabs" : "tabs"}
                            onClick={() => toggleTab(2)}
                        >
                            Vegetable
                        </button>
                    </td>
                    <td>
                        <button
                            className={toggleState === 3 ? "tabs active-tabs" : "tabs"}
                            onClick={() => toggleTab(3)}
                        >
                            Grain
                        </button>
                    </td>
                </div>

                <div className="content-tabs">
                    <div
                        className={toggleState === 1 ? "content  active-content" : "content"}
                    >

                        <div className="search_grid">
                            <table className="table table-bordered">
                                <thead>
                                    <td>
                                        <th scope="col">Meat</th>
                                    </td>
                                </thead>
                                <tbody>
                                    {meatState.map((d, i) => (
                                        <td key={d.id}>
                                            <th scope="col">
                                                <input
                                                    onChange={event => {
                                                        let checked = event.target.checked;
                                                        setmeatState(
                                                            meatState.map(data => {
                                                                if (d.id === data.id) {
                                                                    data.select = checked;
                                                                }
                                                                return data;
                                                            })
                                                        );
                                                    }}
                                                    type="checkbox"
                                                    checked={d.select}
                                                ></input>
                                            </th>
                                            <td>{d.meat}</td>
                                        </td>
                                    ))}
                                </tbody>
                                <tr>

                                    <button
                                        type="button"
                                        className="btn btn-primary btn-sm float-right my-3"
                                        onClick={() => toggleTab(2)}
                                    >
                                        Next
                                    </button>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div
                        className={toggleState === 2 ? "content  active-content" : "content"}
                    >
                        <div className="search_grid">
                            <table className="table table-bordered">
                                <thead>
                                    <td>
                                        <th scope="col">Vegetable</th>
                                    </td>
                                </thead>
                                <tbody>
                                    {vegsState.map((v, j) => (
                                        <td key={v.id}>
                                            <th scope="col">
                                                <input
                                                    onChange={event => {
                                                        let checked = event.target.checked;
                                                        setvegsState(
                                                            vegsState.map(data => {
                                                                if (v.id === data.id) {
                                                                    data.select = checked;
                                                                }
                                                                return data;
                                                            })
                                                        );
                                                    }}
                                                    type="checkbox"
                                                    checked={v.select}
                                                ></input>
                                            </th>
                                            <td>{v.vegs}</td>
                                        </td>
                                    ))}
                                </tbody>
                                <tr>

                                    <button
                                        type="button"
                                        className="btn btn-primary btn-sm float-right my-3"
                                        onClick={() => toggleTab(3)}
                                    >
                                        Next
                                    </button>

                                </tr>
                            </table>
                        </div>
                    </div>











                    <div
                        className={toggleState === 3 ? "content  active-content" : "content"}
                    >
                        <div className="search_grid">
                            <table className="table table-bordered">
                                <thead>
                                    <td>
                                        <th scope="col">Grain</th>
                                    </td>
                                </thead>
                                <tbody>
                                    {grainsState.map((g, k) => (
                                        <td key={g.id}>
                                            <th scope="col">
                                                <input
                                                    onChange={event => {
                                                        let checked = event.target.checked;
                                                        setgrainsState(
                                                            grainsState.map(data => {
                                                                if (g.id === data.id) {
                                                                    data.select = checked;
                                                                }
                                                                return data;
                                                            })
                                                        );
                                                    }}
                                                    type="checkbox"
                                                    checked={g.select}
                                                ></input>
                                            </th>
                                            <td>{g.grains}</td>
                                        </td>
                                    ))}
                                </tbody>
                                <tr>

                                    <button
                                        type="button"
                                        className="btn btn-primary btn-sm float-right my-3"
                                        onClick={() => toggleTab(1)}
                                    >
                                        Next
                                    </button>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <td>


            </td>


            <button className="submit-btn"

                type="button"
        
                onClick={() => {
                    for (let i = 0; i < grainsState.length; i++) {
                        if (grainsState[i].select === true) {
                            result.push(grainsState[i].grains);
                        }
                    }
                    for (let i = 0; i < meatState.length; i++) {
                        if (meatState[i].select === true) {

                            result.push(meatState[i].meat);
                        }
                    }
                    for (let i = 0; i < vegsState.length; i++) {
                        if (vegsState[i].select === true) {
                            result.push(vegsState[i].vegs);
                        }
                    }
                    console.log(
                        result
                    )
                    // call the post data
                    PostData();



                }

                }>
                SUBMIT
            </button>





        </div >

    )
}

export default Search;