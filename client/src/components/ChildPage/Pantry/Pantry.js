
import React, { Component } from 'react'
import NavbarChild from '../NavbarChild/NavbarChild';
import { AppContext } from "../../../AppContext";
import PantryList from './PantryList';
import './Pantry.css'

let userInfo;
var userName="";
class Pantry extends Component {
 
  static contextType = AppContext;


  async componentDidMount() {
    const user = this.context
    let apiCall = "http://localhost:5000/users/email/"+user[0];
    const res = await fetch(apiCall, {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        },
    });

    const data = await res.json();
    userInfo = data
    console.log('test2',data);

     userName = userInfo[0].name
    console.log('test2',userName);

    
}

  constructor(props) {
    super(props);
    this.pantryRef = [];
    this.state = {
      pantryInputCount: 0,
      user: {
        pantry: []
      }
    }
  }
  addPantryInput(e) {
    e.preventDefault();
    var pantrycount = this.state.pantryInputCount;
    this.updatePantry(pantrycount, ",");
    this.setState({ pantryInputCount: pantrycount + 1 });
  }

  updatePantry(index, measurement, ingredient) {
    var user = { ...this.state.user }
    var pantryList = user["pantry"];
    if (index > pantryList.length - 1) {
      pantryList.push({
        ingredient: "",
        measurement: ""
      });
    } else {
      if (measurement) {
        pantryList[index].measurement = measurement;
      }
      else if (ingredient) {
        pantryList[index].ingredient = ingredient;
      }

    }
    this.setState({ user: user });
  }

  updateIngredientValue(e) {
    var value = e.target.value;
    var index = e.target.name;
    // console.log('ID: '+index);
    // console.log('VALUES: '+value);
    this.updatePantry(index, null, value);
  }
  updateMeasurementValue(e) {
    var value = e.target.value;
    var index = e.target.name;
    // console.log('ID: '+index);
    // console.log('VALUES: '+value);
    this.updatePantry(index, value, null);
  }

  async submit() {
    if (!this.state.user.pantry.length) {
      console.log(JSON.stringify(this.state.user.pantry));
      window.alert("No new Ingredients to be added!");
    }
    else {
     
      console.log("value: " + JSON.stringify(this.state.user));
      const res = await fetch(`http://localhost:5000/users/${userName}/pantry`, {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          "Content-Type": "application/json"
        },
        body: JSON.stringify(this.state.user.pantry)

      });
      window.alert("Successfully saved new Ingredients, you can now exit!");
    }
  }
  render() {
    return (
      <div className="createPantryPage" id="create">
        <NavbarChild />
        <h2 className="yourPantry">Your Pantry</h2>

        <div className="table-container">
          <td className="Ingr-table">
            <div className="pantryPage">
              <div className="pantryForm">
                <div className="pantryText" id="ingredientsBox">
                  <label>Ingredients:</label>
                  <br />
                  <ul className="listElements">
                    {this.state.user.pantry.map((ingredientItem, index) => {
                      return (<li key={index} id={index}>
                        <input
                          type="text"
                          placeholder="measurement..."
                          name={index}
                          className="inputRecipeTag"
                          onChange={this.updateMeasurementValue.bind(this)} />
                        <input
                          type="text"
                          placeholder="ingredient..."
                          className="inputRecipeTag"
                          name={index}
                          onChange={this.updateIngredientValue.bind(this)} />
                      </li>);
                    })}
                  </ul>
                  <input type="image" src={process.env.PUBLIC_URL + "/icons/" + "addButtonIcon.png"} onClick={this.addPantryInput.bind(this)} />
                </div>
              </div>
              <button type="button" onClick={this.submit.bind(this)} className="saveRecipe">Save</button>
            </div>
          </td>


          <td><PantryList /></td>
        </div>
      </div>
    )
  }
}

export default Pantry


