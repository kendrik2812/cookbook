import React, {Component} from 'react'
import { AppContext } from "../../../AppContext";
import './Pantry.css'

let userInfo;
var userName="";
class PantryList extends Component {
    static contextType = AppContext;
    state ={
        pantryData:[]
    }

   async componentDidMount (){
    
        const user = this.context
        let apiCall = "http://localhost:5000/users/email/"+user[0];
        const res = await fetch(apiCall, {
            method: "GET",
            headers: {
              "Content-Type": "application/json"
            },
        });
    
        const data = await res.json();
        userInfo = data
        console.log('test5',data);
        console.log('user',user[0]);
    
         userName = userInfo[0].name
        console.log('test5',userName);
        this.getPantryPage();
        
  

    }; 
    getPantryPage = async () =>{
        try{
            const res = await fetch(`http://localhost:5000/users/${userName}/pantry`, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json"
                },
          
            });
             const data = await res.json();

             this.setState({pantryData: data[0].pantry});
             console.log('test',this.state.pantryData);
            
  
         
           console.log(data)
        }catch (err){
            console.log(err);
            
        }
    }
  
    
   render(){
    const listdata = this.state.pantryData.length?this.state.pantryData: [];


    if(listdata)
    {
        return (
            
            <table className = "Pantry" >
                <tr>
                    <th className = "Pantry">IngredientName</th>
                    <th className = "Pantry">Measurement</th> 
                </tr>
                {
                    listdata.map((item, index) => (
                        <tr key={index}>
                           <td className = "Pantry">
                           {item.ingredient}
                           </td> 
                           <td className = "Pantry">
                           {item.measurement}
                           </td>
                        </tr>
                        ))
                }
             </table>
                
    
            
        )
    }
    return null;
   }
  
}
export default PantryList