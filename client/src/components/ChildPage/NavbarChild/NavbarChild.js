import React from "react";
import './NavbarChild.css';
import { useState } from 'react';
import logo from "../../../images/logo.png";
import { Link } from 'react-router-dom';
import MainPage from "../../MainPage/MainPage.js"
function NavbarChild() {

    const [clicked, setClicked] = useState(false);

    return (
        <nav className="NavbarChild">
            <h1 className="navbar-logo"><i className="fab fa-react">
                <img src={logo} width="90px" height="85px" alt="" /></i></h1>
            <div className="menu-icon" onClick={() => setClicked(!clicked)}>
                <i className={clicked ? 'fas fa-times' : 'fas fa-bars'}>
                </i>

            </div>
            <ul className={clicked ? 'nav-menu active' : 'nav-menu'}>
                {

                    <li className="nav-links">
                        <Link to="/search">Search</Link>
                        <Link to="/pantry">Pantry</Link>
                        <Link to="/recipe">Create Recipe</Link>
                        <a href= "/" exact component={MainPage}>Sign Out</a>
                    </li>



                }

            </ul>
        </nav>
    )


}
export default NavbarChild;