import React, { useState, useEffect } from 'react'
import "./DisplayResult.css";
import NavbarChild from "../NavbarChild/NavbarChild.js"
let i = 0;
let j = 1;
let size = 0;


function DisplayResult(props) {
    window.scrollTo(0, 0);
    const hey = props.location.state.result;
    console.log("FROM DISPLAY RESULT PAGE ");
    console.log(hey);
    const [Iterator, setIterator] = useState();
    let value = [];
    if (Array.isArray(hey)) {
        value = hey[i];
        console.log(hey.length);
        size = hey.length;
    }
    else {
        value = hey;
        console.log("FALSE");
        j = props.location.state.num;
        size = 1;
    }
    useEffect(() => {
        //console.log(hey[i]);
        j = j + 1;
    }, [Iterator])

    console.log(value);

    return (

        <div>
            <div className="display-overlay" id="display">
                <NavbarChild></NavbarChild>

                <div className="display-body">
                    <div className="display-recipebox">

                        <h2 className="display-recipe"> RECIPE NAME : {value.recipe_name} <br /> <br /></h2>
                    </div>
                    <div className="display-info">
                        <h3 className="display-h2"> CUISINE TYPE : {value.cuisine_type} <br />
                            PREP TIME : {value.prep_time} <br />
                            INGREDIENTS LIST: </h3>

                        {
                            value.ingredients.map(index => (
                                
                               <tr className="display-ingredient" align="center">  {index.ingredient}&ensp;
                                    <td className="display-ingredient" align="center">&ensp;{index.measurement}</td>
                                    
                                </tr>

                            

                            ))
                        }
                    </div>
                    <h2 className="display-number">Recipe: {j} of {size} </h2>

                </div>

                <button className="submit-btn1"

                    type="button"
                    onClick={() => {
                        i = i + 1;
                        if (i == hey.length) { i = 0; j = 1; }
                        setIterator(i)
                    }

                    }>
                    NEXT
                </button>

            </div>
        </div>
    )
}


export default DisplayResult;
