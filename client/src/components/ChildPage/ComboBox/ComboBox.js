import React, { useState, useEffect } from "react";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useHistory } from "react-router-dom";
import "./ComboBox.css"

let selected = [];

export default function ComboBox() {


  const [data, setData] = useState([]);

  const [input, setInput] = useState('');


  const history = useHistory();


  useEffect(async () => {
    let temp = [];
    const res = await fetch("http://localhost:5000/recipes", {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      },

    })
      .then((value) => value.json())
      .then((result) => {
        result.map((obj) => {
          temp.push(obj);
          // console.log(obj);
        })
      })

    setData(temp)


  }, []);

  const Search = async (e) => {
    console.log(selected);
    history.push({
      pathname: "/displayresult",
      state: {
        result: selected,
        num: 1
      }
    });

  }
  return (
    <div className = "search-auto">
      <Autocomplete
        id="combo-box-demo"
        options={data}
        getOptionLabel={(option) => option.recipe_name}
        onChange={(e, v) => setInput(v)}
        style={{ width: 300 }}

        renderInput={(params) => <TextField {...params} label="Search Recipe" variant="outlined" />}
        onChange={(e, v) => {
          selected = v;
          Search();
        }

        }

      />
    </div>
  )
}

