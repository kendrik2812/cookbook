import React, {useState} from 'react'
import {useHistory} from 'react-router-dom'
import './Recipe.css'
import NavbarChild from '../NavbarChild/NavbarChild';
import { AppContext } from "../../../AppContext";

let userInfo;

class Recipe extends React.Component{

    static contextType = AppContext
    
    async componentDidMount() {
        const user = this.context
        let apiCall = "http://localhost:5000/users/email/"+user[0];
        const res = await fetch(apiCall, {
            method: "GET",
            headers: {
              "Content-Type": "application/json"
            },
        });

        const data = await res.json();
        userInfo = data

        let userName = userInfo[0].name

        var recipe = {...this.state.recipe};
        recipe["chef_name"] = userName;
        this.setState({
            recipe: recipe
        });
    }

    constructor(props){
        super(props);
        this.ingredientsRef = [];
        this.state = {
            ingredientInputCount: 0,
            recipe: {
                recipe_name: "",
                chef_name: "",
                prep_time: "",
                cuisine_type: "",
                ingredients: [],
                instructions: "",
                publication: false
            }
        }
    }

    addIngredientInput(e){
        e.preventDefault();
        var ingredientCount = this.state.ingredientInputCount;
        this.updateIngredient(ingredientCount, '', '');
        this.setState({ingredientInputCount: ingredientCount + 1});
    }

    updateIngredient(index, measurement, ingredient){
        var recipe = {...this.state.recipe}
        var ingredientsList = recipe["ingredients"];
        if(index > ingredientsList.length-1) {
            ingredientsList.push({
                ingredient: '',
                measurement: ''
            });
        }
        else{
            if(measurement) {
                ingredientsList[index].measurement = measurement;
            }
            else if(ingredient) {
                ingredientsList[index].ingredient = ingredient;
            }
            
        }
        this.setState({recipe: recipe});
        // console.log('INGR:' + JSON.stringify(ingredientsList));
    }

    updateIngredientValue(e){
        var value = e.target.value;
        var index = e.target.name;
        // console.log('ID: '+index);
        // console.log('VALUES: '+value);
        this.updateIngredient(index, null, value);
    }

    updateMeasurementValue(e){
        var value = e.target.value;
        var index = e.target.name;
        // console.log('ID: '+index);
        // console.log('VALUES: '+value);
        this.updateIngredient(index, value, null);
    }

    updateRecipe(e)
    {
        var {name, value} = e.target;
        var recipe = {...this.state.recipe};
        // console.log("name:" + name + " " + value);
        recipe[name] = value;
        this.setState({recipe: recipe});
    }


    async submit(){
        
        // console.log("value: " + JSON.stringify(this.state.recipe));
        const res = await fetch("http://localhost:5000/recipes/create", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                "Content-Type": "application/json"
            },
            body: JSON.stringify(this.state.recipe)

        });
        if(res.status == 422)
        {
            window.alert("Please fill out all fields! ");
        }
        else if (res.status == 200)
        {
            window.alert("Succesfully save new Recipe!");
        }
    }

    render() {

        return(
            <div className="createRecipePage" id="create">
                <NavbarChild />
                <div className="recipePage">
                    <div className="recipeForm">
                        <div className="recipeTitle">
                            <input className="recipeName" type="text" placeholder="Recipe Name..." name="recipe_name" onChange={this.updateRecipe.bind(this)}/> 
                            <div className="recipeTag">
                                <img className="recipeTagIcon" src={process.env.PUBLIC_URL+"/icons/"+"chefNameIcon.png"} alt="chef name"></img>
                                <input type="text" placeholder={this.state.recipe.chef_name} disabled={true} name="chef_name" className="inputRecipeTag" onChange={this.updateRecipe.bind(this)}/> 
                            </div>
                        </div>
                        <div className="recipeLabels">
                            <div className="recipeTag">
                                <img className="recipeTagIcon" src={process.env.PUBLIC_URL+"/icons/"+"prepTimeIcon.png"} alt="prep time"></img>
                                <input type="text" placeholder="Prep Time..." name="prep_time" onChange={this.updateRecipe.bind(this)} className="inputRecipeTag"/> 
                            </div>
                            <div className="recipeTag">
                                <img className="recipeTagIcon" src={process.env.PUBLIC_URL+"/icons/"+"cuisineTypeIcon.png"} alt="cuisine type"></img>
                                <input type="text" placeholder="Cuisine Type..." name="cuisine_type" className="inputRecipeTag" onChange={this.updateRecipe.bind(this)}/> 
                            </div>
                            <div className="recipeTag">
                                <img className="recipeTagIcon" src={process.env.PUBLIC_URL+"/icons/"+"publicIcon.png"} alt="private/public"></img>
                                <input type="text" placeholder="Public" name="publication"  className="inputRecipeTag" onChange={this.updateRecipe.bind(this)}/> 
                            </div>
                        </div>
                        <div className="recipeText" id="ingredientsBox">
                            <label>Ingredients:</label>
                            <br/>
                            <ul className="listElements">
                                {this.state.recipe.ingredients.map((ingredientItem, index) => {
                                    return (<li key={index} id={index}>
                                        <input type="text" placeholder="measurement..." name={index} className="inputRecipeTag" onChange={this.updateMeasurementValue.bind(this)} /><input type="text" placeholder="ingredient..." className="inputRecipeTag" name={index} onChange={this.updateIngredientValue.bind(this)}/>
                                    </li>);
                                })}
                            </ul>
                            <input type="image" src={process.env.PUBLIC_URL+"/icons/"+"addButtonIcon.png"} onClick={this.addIngredientInput.bind(this)}/>
                        </div>
                        <div className="recipeText" id="instructionsBox">
                            <label>Instructions:</label>
                            <br/>
                            <textarea name="instructions" onChange={this.updateRecipe.bind(this) } className="instructionsBoxArea"></textarea>
                        </div>
                    </div>
                    <button type="button" onClick={this.submit.bind(this)} className="saveRecipe">Save</button>
                </div>
            </div>
        );
    }

}

export default Recipe
