import React from 'react';


import "./Home.css";
function Home() {
  return (
      <div className="overlay-home" id="home">
        <section className="intro-home"> 
          <div className="content-home">
            <h1 className ="title2">MyCookBook</h1>
          </div>
        </section>
      </div>  
  );
}

export default Home;
